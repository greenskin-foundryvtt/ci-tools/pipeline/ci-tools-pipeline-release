# ci-tools-pipeline-release

This module contains abstract gitlab ci function for common release

## release-common.yml

Since 1.0.0

Abstract ci function to manage release phase with patch, minor or major keyword instead of version

### Usage

release-common must be used with an other release yml
